import React from 'react';
import { Provider } from 'react-redux';
import store from 'store';
import PokemonPage from './modules/pokemon/PokemonPage';

const App: React.FC = () => (
  <Provider store={store}>
    <PokemonPage />
  </Provider>
);

export default App;
