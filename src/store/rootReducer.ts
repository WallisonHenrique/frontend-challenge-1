import { combineReducers } from 'redux';
import pokemon from 'modules/pokemon/store';

export default combineReducers({ pokemon });
