import { all } from 'redux-saga/effects';

import pokemonSaga from 'modules/pokemon/store/sagas';

export default function* rootSaga() {
  yield all([...pokemonSaga]);
}
