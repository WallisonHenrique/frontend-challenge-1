import { render } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import store from 'store';

export const renderCustom = (component: React.ReactNode) =>
  render(<Provider store={store}>{component}</Provider>);
