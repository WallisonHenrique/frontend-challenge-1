import { PokemonStateTypes } from 'modules/pokemon/types/storeTypes';

export type StoreTypes = {
  pokemon: PokemonStateTypes;
};
