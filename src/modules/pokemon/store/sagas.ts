import { call, put, takeEvery } from '@redux-saga/core/effects';
import { errorMessages } from 'constants/errorMessages';
import * as actionsTypes from 'modules/pokemon/store/types';
import { ActionType } from 'typesafe-actions';
import services from '../services/services';
import { PokemonTypes } from '../types/pokemonTypes';
import actionPokemon from './actions';

export function* loadPokemonRequest({
  payload,
}: ActionType<typeof actionPokemon.loadPokemon>) {
  try {
    yield put(actionPokemon.setLoading(true));
    const pokemonData: PokemonTypes = yield call(
      services.getPokemonById,
      payload,
    );
    yield put(actionPokemon.addPokemon(pokemonData));
    yield put(actionPokemon.setError(''));
  } catch (error) {
    const { status }: { status: keyof typeof errorMessages } = error.response;
    yield put(actionPokemon.setError(errorMessages[status]));
  } finally {
    yield put(actionPokemon.setLoading(false));
  }
}

export default [takeEvery(actionsTypes.LOAD_POKEMON, loadPokemonRequest)];
