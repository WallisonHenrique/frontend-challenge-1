import { action } from 'typesafe-actions';
import { PokemonTypes } from '../types/pokemonTypes';
import * as actionTypes from './types';

const loadPokemon = (pokemonId: number) =>
  action(actionTypes.LOAD_POKEMON, pokemonId);

const addPokemon = (pokemonData: PokemonTypes) =>
  action(actionTypes.ADD_POKEMON, pokemonData);

const setError = (error: string) => action(actionTypes.SET_ERROR, error);

const setLoading = (loading: boolean) =>
  action(actionTypes.SET_LOADING, loading);

const actionPokemon = {
  loadPokemon,
  addPokemon,
  setError,
  setLoading,
};

export default actionPokemon;
