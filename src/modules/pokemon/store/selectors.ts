import { StoreTypes } from 'types/storeTypes';

const loading = (state: StoreTypes) => state.pokemon.loading;
const pokemonData = (state: StoreTypes) => state.pokemon.pokemonData;
const warning = (state: StoreTypes) => state.pokemon.error;

const selectorPokemon = {
  loading,
  pokemonData,
  warning,
};

export default selectorPokemon;
