export const LOAD_POKEMON = 'LOAD_POKEMON';
export const ADD_POKEMON = 'ADD_POKEMON';
export const SET_LOADING = 'SET_LOADING';
export const SET_ERROR = 'SET_ERROR';
