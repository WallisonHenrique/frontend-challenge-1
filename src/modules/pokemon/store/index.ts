import { ActionType } from 'typesafe-actions';
import { PokemonStateTypes } from '../types/storeTypes';
import * as actionTypes from './types';
import * as actions from './actions';

export type TodosAction = ActionType<typeof actions>;

const INITIAL_STATE: PokemonStateTypes = {
  error: '',
  loading: false,
  pokemonData: undefined,
};

const reducer = (state = INITIAL_STATE, action: TodosAction) => {
  switch (action.type) {
    case actionTypes.ADD_POKEMON:
      return { ...state, pokemonData: action.payload };
    case actionTypes.SET_ERROR:
      return { ...state, error: action.payload };
    case actionTypes.SET_LOADING:
      return { ...state, loading: action.payload };
    default:
      return state;
  }
};

export default reducer;
