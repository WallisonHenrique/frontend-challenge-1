import { PokemonTypes } from './pokemonTypes';

export type PokemonStateTypes = {
  error: string;
  loading: boolean;
  pokemonData: PokemonTypes | undefined;
};
