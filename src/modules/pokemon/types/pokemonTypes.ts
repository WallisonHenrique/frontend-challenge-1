/* eslint-disable camelcase */
import { palette } from 'styles/palette';

interface SpritesTypes {
  back_default: string;
  back_shiny: string;
  front_default: string;
  front_shiny: string;
}

interface TypeTypes {
  name: keyof typeof palette;
}

interface TypesTypes {
  type: TypeTypes;
}

export interface PokemonTypes {
  id: number;
  name: string;
  sprites: SpritesTypes;
  types: TypesTypes[];
}
