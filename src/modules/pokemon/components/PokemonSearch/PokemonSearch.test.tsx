import React from 'react';
import { screen } from '@testing-library/react';
import { renderCustom } from 'utils/test';
import PokemonSearch from './PokemonSearch';

describe('PokedexPage', () => {
  test('should check that the input field and the search button exist on the screen', () => {
    renderCustom(<PokemonSearch />);
    const buttonElement = screen.getByRole('button', {
      name: /go/i,
    });
    const inputElement = screen.getByLabelText(/busque pelo número/i);
    expect(buttonElement).toBeInTheDocument();
    expect(inputElement).toBeInTheDocument();
  });

  test('should verify that the input field is of type number and minimum equal to 1', () => {
    renderCustom(<PokemonSearch />);
    const inputElement = screen.getByLabelText(/Busque pelo número/i);
    expect(inputElement).toHaveAttribute('type', 'number');
    expect(inputElement.min).toBe('1');
  });
});
