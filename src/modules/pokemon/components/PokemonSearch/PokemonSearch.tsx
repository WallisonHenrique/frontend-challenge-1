import React, { useState } from 'react';
import { Box, Grid, TextField } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import actionPokemon from 'modules/pokemon/store/actions';
import { SearchButton } from './styled';

const PokemonSearch: React.FC = () => {
  const dispatch = useDispatch();

  const [pokemonId, setPokemonId] = useState(1);

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    const pokemonNumber = event.target.search.value;
    dispatch(actionPokemon.loadPokemon(pokemonNumber));
  };

  const handleChange = (event: any) => setPokemonId(event.target.value);

  return (
    <form onSubmit={handleSubmit}>
      <Box mb={8} mt={8}>
        <Grid container justify="center">
          <Grid item>
            <TextField
              id="number"
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{ inputProps: { min: 1 } }}
              label="Busque pelo número"
              name="search"
              onChange={handleChange}
              type="number"
              variant="outlined"
              value={pokemonId}
            />
          </Grid>
          <Grid item>
            <SearchButton
              color="secondary"
              size="large"
              type="submit"
              variant="contained"
            >
              Go
            </SearchButton>
          </Grid>
        </Grid>
      </Box>
    </form>
  );
};

export default PokemonSearch;
