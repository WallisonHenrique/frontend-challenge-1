import { Typography } from '@material-ui/core';
import styled from 'styled-components';

export const Name = styled(Typography)`
  text-transform: capitalize;
`;

export const Sprites = styled.div`
  > div {
    margin: 0 20px;
  }
`;

export const Sprite = styled.div`
  align-items: center;
  display: flex;
  height: 100px;
  justify-content: center;
  position: relative;
  width: 100px;

  &:after {
    background-color: #fff;
    content: '';
    border-radius: 100%;
    height: 80%;
    position: absolute;
    width: 80%;
  }

  & > img {
    height: auto;
    max-width: 100%;
    z-index: 1;
  }
`;
