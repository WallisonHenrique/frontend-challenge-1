import React from 'react';
import { screen } from '@testing-library/react';
import { renderCustom } from 'utils/test';
import { PokemonTypes } from 'modules/pokemon/types/pokemonTypes';
import Pokemon from './Pokemon';

const mockData: PokemonTypes = {
  id: 1,
  name: 'bulbasaur',
  sprites: {
    front_default: 'http://front_default.com',
    back_default: 'http://back_default.com',
    front_shiny: 'http://front_shiny.com',
    back_shiny: 'http://back_shiny.com',
  },
  types: [
    {
      type: {
        name: 'fire',
      },
    },
  ],
};

describe('Pokemon', () => {
  test('should check if the name of the pokémon is displayed', () => {
    renderCustom(<Pokemon pokemon={mockData} />);
    const nameElement = screen.getByRole('heading', {
      name: /1\. bulbasaur/i,
    });
    expect(nameElement).toBeInTheDocument();
  });

  test.each([1, 2, 3, 4])(
    'should check that the sprites are displayed',
    number => {
      renderCustom(<Pokemon pokemon={mockData} />);
      const nameElement = screen.getByAltText(`bulbasaur ${number}`);
      expect(nameElement).toBeInTheDocument();
    },
  );
});
