/* eslint-disable camelcase */
import React from 'react';
import { Grid } from '@material-ui/core';
import { PokemonTypes } from 'modules/pokemon/types/pokemonTypes';
import CustomCard from 'components/CustomCard/CustomCard';
import { Name, Sprite } from './styled';

type Props = {
  pokemon: PokemonTypes;
};

const Pokemon: React.FC<Props> = ({
  pokemon: { id, name, sprites, types },
}: Props) => {
  const listSprites = [
    sprites.front_default,
    sprites.back_default,
    sprites.front_shiny,
    sprites.back_shiny,
  ];

  return (
    <>
      <Grid container spacing={3} justify="center">
        <Grid item xs={12}>
          <Name variant="h4" gutterBottom align="center">
            {`${id}. ${name}`}
          </Name>
        </Grid>
        <Grid item xs={12} container spacing={4} justify="center">
          {listSprites.map((sprite, index) => (
            <Grid item key={sprite}>
              <CustomCard color={types[0].type.name} padding="40px">
                <Sprite>
                  <img
                    src={sprite}
                    alt={`${name} ${index + 1}`}
                    height="100"
                    width="100"
                  />
                </Sprite>
              </CustomCard>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </>
  );
};

export default Pokemon;
