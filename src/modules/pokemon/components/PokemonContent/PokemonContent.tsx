import { Alert } from '@material-ui/lab';
import selectorPokemon from 'modules/pokemon/store/selectors';
import React from 'react';
import { useSelector } from 'react-redux';
import Pokemon from '../Pokemon/Pokemon';

const PokemonContent: React.FC = () => {
  const pokemon = useSelector(selectorPokemon.pokemonData);
  const warning = useSelector(selectorPokemon.warning);

  if (warning) return <Alert severity="error">{warning}</Alert>;

  if (pokemon) return <Pokemon pokemon={pokemon} />;

  return null;
};

export default PokemonContent;
