import React from 'react';
import { screen } from '@testing-library/react';
import { renderCustom } from 'utils/test';
import PokemonContent from './PokemonContent';

describe('PokemonContent', () => {
  test('should check that the alert is not displayed by default', () => {
    renderCustom(<PokemonContent />);
    const alertElement = screen.queryByRole('alert');
    expect(alertElement).not.toBeInTheDocument();
  });

  test('should check that it is not applying the Pokémon by default', () => {
    renderCustom(<PokemonContent />);
    const alertElement = screen.queryByRole('img');
    expect(alertElement).not.toBeInTheDocument();
  });
});
