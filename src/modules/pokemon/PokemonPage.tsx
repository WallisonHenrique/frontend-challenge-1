import React from 'react';
import { Container } from '@material-ui/core';
import Loading from 'components/Loading/Loading';
import { useSelector } from 'react-redux';
import PokemonSearch from './components/PokemonSearch/PokemonSearch';
import PokemonContent from './components/PokemonContent/PokemonContent';
import selectorPokemon from './store/selectors';

const PokemonPage: React.FC = () => {
  const loading = useSelector(selectorPokemon.loading);
  console.log('@Page', loading);
  return (
    <Container>
      <PokemonSearch />
      <PokemonContent />
      <Loading layer="2" open={loading} />
    </Container>
  );
};

export default PokemonPage;
