import api from 'services/api';
import routes from './routes';

const getPokemonById = async (id: number) => {
  const { data } = await api.get(`${routes.pokemon}/${id}`);
  return data;
};

export default {
  getPokemonById,
};
