import React from 'react';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import { renderCustom } from 'utils/test';
import MockAdapter from 'axios-mock-adapter';
import api from 'services/api';
import routes from './services/routes';
import { PokemonTypes } from './types/pokemonTypes';
import PokemonPage from './PokemonPage';

const axiosMock = new MockAdapter(api);
const mockData: PokemonTypes = {
  id: 1,
  name: 'bulbasaur',
  sprites: {
    front_default: 'http://front_default.com',
    back_default: 'http://back_default.com',
    front_shiny: 'http://front_shiny.com',
    back_shiny: 'http://back_shiny.com',
  },
  types: [
    {
      type: {
        name: 'fire',
      },
    },
  ],
};

afterEach(cleanup);

describe('PokemonPage', () => {
  test('deve verificar se o loading não é exibido inicialmente', () => {
    renderCustom(<PokemonPage />);
    const loadingElement = screen.getByRole('alert', {
      hidden: true,
    });
    expect(loadingElement).not.toBeVisible();
  });

  test('deve verificar se o formulário de pesquisa é exibido', () => {
    renderCustom(<PokemonPage />);
    const inputElement = screen.getByLabelText(/busque pelo número/i);
    expect(inputElement).toBeInTheDocument();
  });

  test('deve verificar se o aviso não é exibido inicialmente', () => {
    renderCustom(<PokemonPage />);
    const alertElement = screen.queryByRole('alert');
    expect(alertElement).not.toBeInTheDocument();
  });

  test('deve verificar se o pokémon não é exibido inicialmente', () => {
    renderCustom(<PokemonPage />);
    const nameElement = screen.queryByRole('img');
    expect(nameElement).not.toBeInTheDocument();
  });

  // test('deve verificar se apresenta o load ao submeter o formulário', async () => {
  //   axiosMock.onGet(`${routes.pokemon}/1`).reply(200, mockData);
  //   renderCustom(<PokemonPage />);
  //   const buttonElement = screen.getByRole('button', {
  //     name: /go/i,
  //   });
  //   await fireEvent.click(buttonElement);
  //   const nameElement = await screen.getByText(/bulbasaur/i);
  //   expect(nameElement).toBeInTheDocument();
  // });
});
