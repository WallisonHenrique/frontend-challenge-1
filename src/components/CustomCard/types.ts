import React from 'react';
import { palette } from 'styles/palette';

export interface CustomCardTypes {
  children: React.ReactNode;
  color: keyof typeof palette;
  padding: string;
}
