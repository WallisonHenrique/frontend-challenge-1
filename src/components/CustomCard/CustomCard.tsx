import React from 'react';
import { Container } from './styled';
import { CustomCardTypes } from './types';

const CustomCard: React.FC<CustomCardTypes> = ({
  children,
  color,
  padding,
}: CustomCardTypes) => (
  <Container color={color} padding={padding}>
    {children}
  </Container>
);

export default CustomCard;
