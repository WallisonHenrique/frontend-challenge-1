import styled from 'styled-components';
import { palette } from 'styles/palette';
import { CustomCardTypes } from './types';

export const Container = styled.div<CustomCardTypes>`
  border-radius: 20px;
  background-color: ${({ color }) => palette[color]};
  padding: ${({ padding }) => padding};
`;
