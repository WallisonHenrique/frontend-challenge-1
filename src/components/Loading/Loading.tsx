import React from 'react';
import { CircularProgress } from '@material-ui/core';
import { StyledBackdrop } from './styled';

type LoadingTypes = {
  open: boolean;
  layer: string;
};

const Loading: React.FC<LoadingTypes> = ({ open, layer }: LoadingTypes) => (
  <StyledBackdrop layer={layer} open={open} role="alert">
    <CircularProgress color="inherit" />
  </StyledBackdrop>
);

export default Loading;
