import { Backdrop } from '@material-ui/core';
import styled from 'styled-components';

export const StyledBackdrop = styled(Backdrop)`
  z-index: ${({ layer }: { layer: string }) => layer};
`;
