# Desafio Frontend

## Instalação
Clone o repositório frontend-challenge-1 com ssh -> git@gitlab.com:WallisonHenrique/frontend-challenge-1.git ou https -> https://gitlab.com/WallisonHenrique/frontend-challenge-1.git.

Abra o terminal na raíz do projeto e execute o comando `yarn install`. Após a instalação dos pacotes do node execute o comando `yarn start` para rodar o projeto.
## Testes
Para rodar os testes vá para a raíz do projeto e execute o comando `yarn test`.

## Tecnologias
Fiz uso dos seguintes recursos:
- React
- Material Ui e Styled Components
- Redux, Saga e Typesafe Actions
- Testing Library e Jest
- Axios
- Eslint e Prettier
- TypeScript

## Requisitos
- Utilizar o React: estou utilizando a biblioteca React instalada com o CRA mais o TypeScript.
- Implementar testes unitários com Testing Library: implementei alguns testes(não o quanto eu queria devido ao tempo) para os componentes PokemonPage, PokemonContent, PokemonSearch e Pokemon.
- Biblioteca extra: implementei os recursos citados acima. Poderia ter implementado rotas, formik e i18n, mas o aplicativo é pequeno. 
- Use sua criatividade: fiz um layout diferente do proposto nas instruções.
- Readme relevante: adicionei ao meu README as instruções de instalação, tecnologias e como atendi aos requisitos.
- Boas práticas de programação: adicionei eslint, prettier, redux, organização de pastas, reaproveitamento de tipagem, tratamento de erro, loading para melhor experiência do usuário, reutilização de código, typescript, testes, styled components e material ui pensando em um projeto escalável de grande porte.


## Bonus
Como citei acima gostaria de ter utilizado i18next, formik e rotas para mostrar meu conhecimentos. Também gostaria de ter implementado testes para verificar se as requisições estavam sendo feitas corretamentes e ter testado os serviços. Infelizmente meu teste com axios mock(já implementei em projetos reais) falhou e devido ao prazo não tive tempo de corrigi-lo.